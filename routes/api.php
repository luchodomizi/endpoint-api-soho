<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\EtiquetaController;
use App\Http\Controllers\API\ProductoController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


//Rutas para obtener todos los objetos
Route::get('productos', [ProductoController::class, 'index']);

//Rutas para mostrar productos o etiquetas individualmente por id, con un mensaje json de error en caso de no existir el producto solicitado, mediante Eloquent y Validation de laravel. Código en app/Exceptions/Handler.php
Route::get('productos/{producto}', [ProductoController::class, 'show']);

