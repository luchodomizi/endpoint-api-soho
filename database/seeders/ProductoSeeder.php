<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('productos')->insert([[
            'titulo'=> 'Sitio público y privado',
            'descripcion' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent condimentum dignissim lobortis. Integer lobortis eget metus vitae hendrerit. Nam viverra consequat orci a rhoncus. Maecenas hendrerit ut risus id scelerisque. ',
            'img_logo' => 'https://i.ibb.co/L6Zzbrx/logo1.png',
            'img_side' => 'https://i.ibb.co/6gjTCKn/producto1.png',
            'etiquetas' => 'usabilidad, ui, ux, test con usuarios'
        ],
        [
            'titulo'=> 'Sitios Web 2017',
            'descripcion' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent condimentum dignissim lobortis. Integer lobortis eget metus vitae hendrerit. Nam viverra consequat orci a rhoncus. Maecenas hendrerit ut risus id scelerisque. ',
            'img_logo' => 'https://i.ibb.co/KLPkYMr/logo2.png',
            'img_side' => 'https://i.ibb.co/ssfjRCr/producto2.png',
            'etiquetas' => 'responsive, ui, ux'
        ],
        [
            'titulo'=> 'TV App',
            'descripcion' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent condimentum dignissim lobortis. Integer lobortis eget metus vitae hendrerit. Nam viverra consequat orci a rhoncus. Maecenas hendrerit ut risus id scelerisque.',
            'img_logo' => 'https://i.ibb.co/C2rbQd6/logo3.png',
            'img_side' => 'https://i.ibb.co/mqWfv4X/producto3.png',
            'etiquetas' => 'usabilidad, ui, ux, test con usuarios'
        ]]);
    }
}
