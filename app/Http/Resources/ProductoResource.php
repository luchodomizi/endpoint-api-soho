<?php

namespace App\Http\Resources;

use Illuminate\Support\Str;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'titulo' =>Str::upper($this->titulo),
            'descripcion' => $this->descripcion,
            'etiquetas' => $this->etiquetas,
            'img_logo' => $this->img_logo,
            'img_side' => $this->img_side,
        ];
    }
}
