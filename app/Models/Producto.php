<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    use HasFactory;

    protected $fillable = [
        'titulo',
        'descripcion',
        'etiquetas',
        'img_logo',
        'img_side'
    ];

    //Campos que no se van a mostrar
    protected $hidden = [
        'created_at',
        'updated_at'
    ];
}
