<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

- PHP 7.4.9
- MySQL 8.0
- Laravel 8

# Endpoint REST Laravel para prueba técnica SOHO

- clone https://gitlab.com/luchodomizi/endpoint-api-soho.gito
- corra 'composer update'
- configure el .ENV y cree la base de datos
- corra 'php artisan migrate --seed'
- asegurese que la ruta de consumo en el frontend sea correcta


Plus: Utilicé Eloquent para generar algunos avisos personalizados por si la conexión no fuera exitosa, además utilice API Resources para retornar los títulos de productos en mayúsculas.

Humberto Domizi
